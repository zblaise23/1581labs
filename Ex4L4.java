import java.util.Scanner;

public class Ex3L4{
    
    public static void main(String[] args){
        
        Scanner input = new Scanner(System.in);
        int option = 0; 
        int number;
        
        
        //question 1
        System.out.println("enter a number between 0 and 10");
        number = input.nextInt();
        
        if(number > 0 && number < 10){
            System.out.println(number + " is a valid number");
        }
        else{
            System.out.println(number + " is not a valid number");
            
        }
        //question 2
        System.out.println("enter a number divisible by 2 or 3");
        number = input.nextInt();
        
        if(number % 2 ==0 || number % 3 == 0){
            System.out.println(number + " is a valid number");
        }
        else{
            System.out.println(number + " is not a valid number");
            
        }
        //question 3
    }
}