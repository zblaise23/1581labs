import java.util.Scanner;

public class Ex2L3
{
    public static void main( String[] args )
    {
        Scanner input = new Scanner(System.in);
        
        int age; 
        
        System.out.println("Enter your age");
        age = input.nextInt();
        
        System.out.println("before the if statement");
        
        if(age >= 21){
            System.out.println("yep you can have drink");
        }
        else{
            System.out.println("slow down kid!");
        }
        
        System.out.println("after the if statement");
    }
}