import java.util.Scanner;

public class Ex3L4{
    
    public static void main(String[] args){
        
        Scanner input = new Scanner(System.in);
        int option = 0; 
        int number;
        
        while(option != 4){
            System.out.println("*********************************************************************");
            System.out.println(" 1. count to a number | 2. check if even/odd | 3. print hey | 4. QUIT");
            System.out.println("*********************************************************************");
            
            option = input.nextInt(); 
            
            switch(option){
                case 1:
                    
        
                    System.out.println("enter a number to count to");
                    number = input.nextInt();
                    
                    for(int c = 0; c < number; c = c + 1){
                        System.out.println(c + " iterations of the loop");
                    }
                    break;
                
                case 2:
                    //checks if number is even or odd
                    
                    System.out.println("enter a number to check even/odd"); 
                    number = input.nextInt();
                    
                    if(number % 2 == 0){
                        System.out.println(number + " is even");
                    }
                    else if(number % 2 == 1){
                        System.out.println(number + " is odd");
                    }
                    else{
                        System.out.println(number + " must be a crazy number");
                    }
                    break;
                
                case 3:
                    System.out.println("hello this is option:" + option);
                    break;
                
                case 4:
                    //this case breaks loop
                    System.out.println("Leaving the menu");
                    break; // this exits the switch without
                
                default:
                    System.out.println("You entered an invalid option");
                    break;
            }
        }      
    }
}    