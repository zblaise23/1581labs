import java.util.Date;

public class EventTester{
    public static void main(String[] args)
    {
        Date date = new Date();
        
        SchoolEvent party = new SchoolEvent("party", "MATH 209", date, "CSCI-1581");
        
        System.out.println(party);
    }
}