import java.util.Scanner;
import java.util.Formatter;
import java.io.File;

public class Ex2Lab7{
    public static void main(String[] args) throws Exception
    {
        File text = new File("example.txt");
        
        Scanner input = new Scanner(text);
        
        Formatter output = new Formatter("sentence .txt");
        
        int index;
        String firstLine;
        String sentence;
        
        firstLine = input.nextLine();
        System.out.println("firstLine is: " + firstLine);
        
        index = firstLine.indexOf(".");
        
        sentence = firstLine.substring(0, index + 1);
        System.out.println("sentence is: " + sentence);
        
        output.format(sentence);
        
        input.close();
        output.close();
    }
}