import java.util.Scanner;

public class Ex1Lab7
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        
        String s1, s2;
        
        String sentinel = "Anthing But -1";
        
        while(!sentinel.equals("-1"))
        {
            System.out.println("Enter First String");
            s1 = input.nextLine();
            
            System.out.println("Enter Second String");
           s2 = input.nextLine();
        
            if(s1.equals(s2))
            {
                System.out.println("s1 equals s2 by equals method");
            }
            else
            {
                System.out.println("s1 does not equal s2 by equals method");
                
                if(s1.compareTo(s2) == 0)
                {
                    System.out.println("s1 equals s2 by compareTo method");
                }
                else
                {
                    System.out.println("s1 does NOT equal s2 by compareTo method");
                }
                if(s1.length() == s2.length())
                {
                    System.out.println("s1 equals s2 by length method");
                }
                else
                {
                    System.out.println("s1 does NOT equals s2 by length method");
                }
                
                System.out.println("Press Enter to continue or Enter -1 to quit");
                sentinel = input.nextLine();
            }
        }
    }
}