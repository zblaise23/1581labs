import java.util.Scanner;

public class Ex4L3
{
    public static void main( String[] args )
    {
        Scanner input = new Scanner(System.in);
        
        int numberOfGrades; 
        float average;
        float totalGrades = 0;
        float grade;
        
        System.out.println("Enter number of grades");
        
        numberOfgrades = input.nextInt();
        
        System.out.println("before the while statement");
       
        int c = 0;
        while(c < numberOfgrades){
            System.out.println("enter a grade");
            grade = input.nextFloat();
            
            totalGrades = totalGrades + grade;
            
            c = c + 1;
        }
        
        System.out.println("after the while statement");
        
        average = totalGrades / (float) numberOfgrades;
        
        System.out.println("average of grades is: " + average);
    }
}