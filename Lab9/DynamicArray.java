import java.util.Arrays;

public class DynamicArray{
    
    //Instance Variables
    private String[] Data;
    private int size;
    private int currentIndex;
    
    //basic constructor
    public DynamicArray(){
        data = new String [10];
    }
    public DynamicArray(String[] strArray){
        data = Arrays.copyOf(strArray, strArray.length);
        size = strArray.length;
        currentIndex = strArray.length;
    }
    // adds a string to the next element of array
    public void add(String str){
        if(currentIndex >= this.data.length){
            this.expandArray();
        }
        this.data[currentIndex] = str;
        this.currentIndex = this.currentIndex + 1;
        this.size = this.size + 1;
    }
    
    private void expandArray(){
        this.data = Arrays.copyOf(this.data,(int) (this.data.length * 1.5));
    }
    //adds element at specific index
    public void add(String str, int index){
        if(currentIndex >= this.data.length){
            this.expandArray();
        }
        
        if(index < 0 || index >= this.size){
            System.out.println("Index out of bounds error: " + index);
        }
        else{
        
            String newTemp = this.data[index];
            String oldTemp;
            this.data[index] = str;
            
            for(int i =  index + 1; i < this.data.length; i = i + 1){
                oldTemp = this.data[i];
                this.data[i] = newTemp;
                newTemp = oldTemp;
            }
            
            this.currentIndex = this.currentIndex + 1;
            this.size = this.saize + 1;
        }
    }
    
    public void remove(int index){
        if(index < 0 || index >= this.size){
            System.out.println("Index out of bounds error: " + index);
        }
        else{
            for(int i = index; i < this.data.length - 1; i = i + 1){
                this.data[i] = this.data[i + 1];
            }
            
            this.currentIndex = this.currentIndex - 1;
            this.size = this.size - 1;
        }
    }
    
    public String get(int index){
        String element = null;
        
        if(index < 0 || index >= this.size){
            System.out.println("Index out of bounds error: " + index);
        }
        else{
            element = this.data[index];
        }
        
        return element;
    }
    
    public boolean isEmpty(){
        if(this.size == 0){
            return true;
        }
        else{
            return false;
        }
    }
    
    public int sizeOf(){
        return this.size;
    }
    
    public String toString(){
        String arrayStr = "| ";
        
        for(int i = 0; i < this.size; i = i + 1){
            arrayStr = arrayStr + this.data[i] + " | ";
        }
        return arrayStr;
    }
}   
       
       